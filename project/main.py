"""Mail merge project"""

OUTPUT_DIR = "project/Output/ReadyToSend/"

with open("project/Input/Letters/starting_letter.txt") as starting_letter_file:
    starting_letter = starting_letter_file.read()

with open("project/Input/Names/invited_names.txt") as invited_names_file:
    for name in invited_names_file.readlines():
        ready_to_send = starting_letter.replace("[name]", name.strip())
        ready_to_send_file = OUTPUT_DIR + name.strip() + ".txt"
        with open(ready_to_send_file, mode="w") as file:
            file.write(ready_to_send)
